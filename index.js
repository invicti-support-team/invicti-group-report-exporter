const requester = require("axios");
const prompts = require("prompts");
const fs = require("fs");
const { default: axios } = require("axios");
const os = require("os");
const path = require("path");

const getCredentials = async () => {
  if (!fs.existsSync("credentials.json")) {
    console.log(
      "Credentials.json file not found. Please provide the API credentials."
    );
    const result = await prompts([
      {
        type: "text",
        name: "baseUrl",
        message: "Base URL",
        initial: "https://www.netsparkercloud.com/",
      },
      {
        type: "text",
        name: "userId",
        message: "User ID",
      },
      {
        type: "text",
        name: "apiToken",
        message: "API Token",
      },
    ]);
    return result;
  } else {
    try {
      const creds = JSON.parse(fs.readFileSync("credentials.json"));
      console.log("Using credentials.json for API credentials");
      return creds;
    } catch (error) {
      console.error("Error parsing credentials.json:", error);
      throw error;
    }
  }
};

const getAllScanGroups = (axios) => {
  return new Promise(async (resolve, reject) => {
    try {
      let allItems = [];
      let pageNumber = 1;
      let response;
      do {
        response = await axios.get(
          `/api/1.0/websitegroups/list?PageCount=1&PageNumber=${pageNumber}&PageSize=20`
        );
        allItems = allItems.concat(response.data.List);
        pageNumber++;
      } while (pageNumber <= response.data.TotalItemCount / 20);
      resolve(allItems);
    } catch (error) {
      console.log(error.response.data);
    }
  });
};

const groupPicker = (groups) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { groupIds } = await prompts({
        type: "multiselect",
        name: "groupIds",
        message: "Select website groups",
        choices: groups.map((group) => ({
          title: group.Name,
          value: group.Id,
        })),
      });
      resolve(groupIds);
    } catch (error) {
      reject(error);
    }
  });
};

const getWebsitesByGroupId = (axios, groupId) => {
  return new Promise(async (resolve, reject) => {
    try {
      let allItems = [];
      let pageNumber = 1;
      let response;
      do {
        response = await axios.get(
          `/api/1.0/websites/getwebsitesbygroup?query=${groupId}&PageNumber=${pageNumber}&pageSize=20`
        );
        allItems = allItems.concat(response.data.List);
        pageNumber++;
      } while (pageNumber <= response.data.TotalItemCount / 20);
      resolve(allItems);
    } catch (error) {
      reject(error.response.data);
    }
  });
};

const getWebsitesByGroupIds = async (axios, groupIds) => {
  const allItems = [];
  for (const groupId of groupIds) {
    const items = await getWebsitesByGroupId(axios, groupId);
    allItems.push(...items);
  }
  return allItems;
};

// get most recent scan for each website

const getScanByWebsiteUrl = (axios, websiteUrl) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response;
      response = await axios.get(
        `/api/1.0/scans/listbywebsite?websiteUrl=${websiteUrl}&PageNumber=1&pageSize=20&initiatedDateSortType=Descending`
      );
      scan = response.data.List[0];

      resolve(scan);
    } catch (error) {
      reject(error.response.data);
    }
  });
};

const getAllScansByWebsiteUrls = async (axios, websiteUrls) => {
  const allItems = [];
  for (const websiteUrl of websiteUrls) {
    const items = await getScanByWebsiteUrl(axios, websiteUrl);
    allItems.push(items);
  }
  return allItems;
};

const pickReportOptions = async () => {
  const reportOptions = [
    {
      name: "contentFormat",
      type: "select",
      options: ["Html", "Markdown"],
      hint: "Report content format",
    },
    {
      name: "excludeResponseData",
      type: "boolean",
      hint: "Exclude response data",
    },
    {
      name: "format",
      type: "select",
      options: ["Xml", "Csv", "Pdf", "Html", "Txt", "Json"],
      hint: "Report file format",
    },
    {
      name: "type",
      type: "select",
      options: [
        "Crawled",
        "Scanned",
        "Vulnerabilities",
        "ScanDetail",
        "ModSecurityWafRules",
        "OwaspTopTen2013",
        "HIPAACompliance",
        "Pci32",
        "KnowledgeBase",
        "ExecutiveSummary",
        "FullScanDetail",
        "OwaspTopTen2017",
        "CustomReport",
        "Iso27001Compliance",
        "F5BigIpAsmWafRules",
        "WASC",
        "SansTop25",
        "Asvs40",
        "Nistsp80053",
        "DisaStig",
        "OwaspApiTop10",
        "OwaspTopTen2021",
        "VulnerabilitiesPerWebsite",
        "OwaspApiTopTen2023",
        "PciDss40",
      ],
      hint: "Report type",
    },
    {
      name: "onlyConfirmedIssues",
      type: "boolean",
      hint: "Only confirmed issues",
    },
    {
      name: "onlyUnconfirmedIssues",
      type: "boolean",
      hint: "Only unconfirmed issues",
    },
    {
      name: "excludeAddressedIssues",
      type: "boolean",
      hint: "Exclude address issues",
    },
    {
      name: "excludeHistoryOfIssues",
      type: "boolean",
      hint: "Exclude history of issues",
    },
  ];
  let results = [];
  for (const element of reportOptions) {
    if (element.type === "select") {
      const result = await prompts({
        type: "select",
        name: element.name,
        message: `Rerpot Options: ${element.name}`,
        choices: element.options,
        hint: element.hint,
      });
      results.push({
        name: element.name,
        value: element.options[result[element.name]],
      });
    }
    if (element.type === "boolean") {
      const result = await prompts({
        type: "confirm",
        name: element.name,
        message: `Rerpot Options: ${element.name}`,
        hint: element.hint,
      });

      results.push({ name: element.name, value: result[element.name] });
    }
  }
  return results;
};

const downloadReport = async (axios, scanId, reportOptions, reportDir) => {
  try {
    const format = reportOptions.find((option) => option.name === "format");
    let query = reportOptions
      .map((option) => `${option.name}=${option.value}`)
      .join("&");
    query += `&id=${scanId}`;
    let options = {};
    if (format.value.toLowerCase() === "pdf") {
      options = {
        responseType: "stream",
      };
    }
    const response = await axios.get(
      "/api/1.0/scans/report/?" + query,
      options
    );

    if (format.value.toLowerCase() === "pdf") {
      const filePath = path.join(
        reportDir,
        `${scanId}.${format.value.toLowerCase()}`
      );
      const writer = fs.createWriteStream(filePath);
      // console.log(response.data);
      response.data.pipe(writer);
      return new Promise((resolve, reject) => {
        writer.on("finish", resolve);
        writer.on("error", reject);
      });
    } else {
      const filePath = path.join(
        reportDir,
        `${scanId}.${format.value.toLowerCase()}`
      );
      fs.writeFileSync(filePath, response.data);
      return true;
    }
  } catch (error) {
    console.error(
      `Error downloading report for scanId ${scanId}: ${JSON.stringify(
        error.response.data
      )}`
    );
    return false;
  }
};

const downloadReports = async (axios, scanIds, reportOptions) => {
  const reportTime = new Date();
  const reportDir = path.join(
    os.homedir(),
    "Documents",
    "Invicti Report Exporter",
    reportTime.toISOString().split("T")[0],
    reportTime.toISOString().split("T")[1].split(":").slice(0, 2).join("_")
  );

  try {
    await fs.promises.mkdir(reportDir, { recursive: true });

    const allItems = [];

    for (const scanId of scanIds) {
      try {
        const items = await downloadReport(
          axios,
          scanId,
          reportOptions,
          reportDir
        );
        allItems.push(items);
      } catch (error) {
        console.error(
          `Error downloading report for scanId ${scanId}: ${error}`
        );
        allItems.push(false);
      }
    }

    return reportDir;
  } catch (error) {
    console.error(`Error creating report directory: ${error}`);
    throw error;
  }
};

async function main() {
  const creds = await getCredentials();

  const axios = requester.create({
    baseURL: creds.baseUrl,
    auth: {
      username: creds.userId,
      password: creds.apiToken,
    },
  });
  const groups = await getAllScanGroups(axios);
  const selectedGroups = await groupPicker(groups);
  const websites = await getWebsitesByGroupIds(axios, selectedGroups);
  // pass RootUrl
  let scans = await getAllScansByWebsiteUrls(
    axios,
    websites.map((w) => w.RootUrl)
  );
  // Remove undefined scans
  scans = scans.filter((scan) => scan !== undefined);
  const reportOptions = await pickReportOptions();
  // pass scan Ids
  // console.log(scans);
  const scanReportsCompleted = await downloadReports(
    axios,
    scans.map((s) => s.Id),
    reportOptions
  );
  console.log(`Reports downloaded to ${scanReportsCompleted}`);
  // Wait for input before exiting
  return;
}

async function runForever() {
  while (true) {
    await main();
  }
}

runForever();

// Catch process termination none 0 and wait for user input
