# Invicti Report Exporter

This script is an Invicti Report Exporter that allows you to fetch scan groups, select specific groups, retrieve websites by group IDs, retrieve scan reports by website URLs, select report options, and download reports.

## Development

1. Install Node.js from [https://nodejs.org/](https://nodejs.org/)
2. Clone the repository and run `npm install` to install the required dependencies.

## Usage

1. Download the latest artifact from [here](https://gitlab.com/invicti-support-team/invicti-group-report-exporter/-/artifacts)
2. Extract the downloaded file
3. Run the application
4. Follow the on-screen instructions

## Dependencies

- axios: For making HTTP requests
- prompts: For interactive command-line prompts
- fs: For file system operations
- os: For operating system-related utility functions
- path: For working with file and directory paths